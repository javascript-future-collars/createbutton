const openAlert = () => {
  window.alert("super");
};

const createButton = () => {
  const button = document.createElement("button");
  button.id = "super-button";
  button.innerText = "Click me!";
  document.body.appendChild(button);
  button.addEventListener("click", openAlert);
  return button;
};

createButton();
